package main

import (
	"errors"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
)

func GetClaim(w http.ResponseWriter, r *http.Request) (jwt.MapClaims, error) {
	claims := context.Get(r, USER_CLAIMS)
	if claims == nil {
		ServiceUnavailableError("Cannot fetch current user").SendResponse(w)
		return jwt.MapClaims{}, errors.New("Cannot decode claims")
	}

	return claims.(jwt.MapClaims), nil
}
