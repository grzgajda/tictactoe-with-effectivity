# TicTacToe with Effectivity

[![pipeline status](https://gitlab.com/grzgajda/tictactoe-with-effectivity/badges/master/pipeline.svg)](https://gitlab.com/grzgajda/tictactoe-with-effectivity/commits/master) [![coverage report](https://gitlab.com/grzgajda/tictactoe-with-effectivity/badges/master/coverage.svg)](https://gitlab.com/grzgajda/tictactoe-with-effectivity/commits/master)

Simple RESTful API built for Effecitivity's team for playing with new languages. Using API does not require high skill in developing and gives many possibilites for developers like:

* TicTacToe tournament between users
* Dashboard with analytics how user played games
* Possibilitites to make bot who plays with another users

## Usage

Download artifact from GitLab's download menu, choose one of built artifacts (*compile_unix* or *compile_windows*). In the same directory, create file `.env` and fill it with values from `.env.dist`, where:

* `API_MYSQL_NAME` is name of your database.
* `API_MYSQL_USER` is MySQL's user
* `API_MYSQL_PASS` is MySQL's password
* `API_HTTP_PORT` is port for your application
* `APP_ENV` is defined environment, where is `dev`, logging to CLI is enabled
* `APP_JWT_KEY` is secret key to hashing JSON Web Token

After configuring `.env` file, in your Terminal app just run application (`./compile_unix` or `./compile_windows.exe`). Your server should start and url will be `http://localhost:YOUR_API_HTTP_PORT`.

## Authentication

Generate your **JWT** from endpoint `POST /auth` and send next requests with header `Authentication: Bearer YOUR_TOKEN`. Remember that each authentication generate you a new JWT.

## Endpoints

`POST /auth` endpoint to authenticate new user, requires payload similar to this: `{"username": "YOUR_USERNAME", "password": "YOUR_PASSWORD"}`. Endpoint returns newly generated JSON Web Token for authentication.

`GET /users` retrieve all users from DB (does not require authentication), list of users contains minimum information about users.

`GET /games` retrieve all games where authenticad user participated (or is participing). List does not contain any response limiting so with many played games, it is possible to return thousands of elements.

`POST /games` create new game with another user. To create game, it's required to send JSON request with body similar to this: `{"play_with": "YOUR_ENEMY_USER_ID"}`.

`POST /games/{GAME_ID}` place your mark on TicTacToe's board, request requires JSON payload with body: `{"x": "YOUR_X_POSITION", "y": "YOUR_Y_POSITION"}` where X, Y values should be from range 0-2.