package main

import "time"

type User struct {
	ID            uint           `json:"id" gorm:"primary_key"`
	Username      string         `json:"username" gorm:"size:50;not null"`
	Slug          string         `json:"slug" gorm:"size:50;not null"`
	Password      string         `json:"-" gorm:"not null"`
	Points        int64          `json:"points"`
	HistoryPoints []HistoryPoint `json:"history_points,omitempty"`
	WonGames      []Game         `json:"won_games" gorm:"ForeignKey:WinnerID"`
	CreatedAt     time.Time      `json:"-"`
	UpdatedAt     time.Time      `json:"-"`
	DeletedAt     *time.Time     `json:"-" sql:"index"`
}

func (User) TableName() string {
	return "users"
}
