package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/jameskeane/bcrypt"
	"github.com/jinzhu/gorm"
)

func Authenticate(db *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var authData AuthenticationRequest
		json.NewDecoder(r.Body).Decode(&authData)

		if len(authData.Username) == 0 {
			BadRequestError("Please provide username to authentication").SendResponse(w)
			return
		}
		if len(authData.Password) == 0 {
			BadRequestError("Please provide password to authentication").SendResponse(w)
			return
		}

		var user User
		db.Where("username = ?", authData.Username).First(&user)

		if bcrypt.Match(authData.Password, user.Password) {
			token, err := GetJSONWebTokenLoggable(user.ID, user.Username, user.Slug)
			if err != nil {
				InternalServerError("Error generating JWT token: " + err.Error()).SendResponse(w)
			} else {
				w.Header().Set("Authorization", "Bearer "+token.Token)
				w.Header().Set("Content-type", "application/json")
				w.WriteHeader(http.StatusOK)

				json.NewEncoder(w).Encode(token)
			}
		} else {
			UnauthorizedError("Name and password does not match").SendResponse(w)
		}
	}
}

func IndexUsers(db *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var users []User
		db.
			Preload("HistoryPoints").
			Preload("WonGames").
			Order("points desc").
			Find(&users)

		var jsonUsers []IndexUsersResponse
		for i := range users {
			jsonUser := IndexUsersResponse{
				ID:       users[i].ID,
				Username: users[i].Username,
				Slug:     users[i].Slug,
				Points:   users[i].Points,
			}
			jsonUsers = append(jsonUsers, jsonUser)
		}

		json.NewEncoder(w).Encode(jsonUsers)
	}
}

func IndexCurrentUser(db *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser, err := GetClaim(w, r)
		if err != nil {
			InternalServerError("Cannot fetch data about user: " + err.Error())
			return
		}

		var user User
		db.
			Preload("HistoryPoints").
			Preload("WonGames").
			Where("slug = ?", currentUser["userslug"]).
			First(&user)

		json.NewEncoder(w).Encode(user)
	}
}

func IndexGames(db *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser, err := GetClaim(w, r)
		if err != nil {
			InternalServerError("Cannot fetch data about user: " + err.Error())
			return
		}

		var games []Game
		db.
			Preload("PlayerOne").
			Preload("PlayerTwo").
			Preload("Winner").
			Preload("Moves").
			Where("player_one_id = ?", currentUser["id"]).
			Or("player_two_id = ?", currentUser["id"]).
			Find(&games)

		json.NewEncoder(w).Encode(games)
	}
}

func CreateGame(db *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser, err := GetClaim(w, r)
		if err != nil {
			InternalServerError("Cannot fetch data about user: " + err.Error())
			return
		}
		var requestBody CreateGameRequest
		json.NewDecoder(r.Body).Decode(&requestBody)
		if requestBody.PlayWithID == 0 {
			BadRequestError("Sent invalid player ID").SendResponse(w)
			return
		}

		if uint(currentUser["id"].(float64)) == requestBody.PlayWithID {
			BadRequestError("You cannot start game with yourself!").SendResponse(w)
			return
		}

		game := Game{
			IsFinished:  false,
			PlayerOneID: uint(currentUser["id"].(float64)),
			PlayerTwoID: uint(requestBody.PlayWithID),
		}

		var lastGame Game
		db.
			Where("is_finished = false AND player_one_id = ? AND player_two_id = ?", currentUser["id"], requestBody.PlayWithID).
			First(&lastGame)

		if lastGame.PlayerOneID == game.PlayerOneID {
			BadRequestError("You have a game with this player!").SendResponse(w)
			return
		}

		db.Create(&game)
		json.NewEncoder(w).Encode(game)
	}
}

func PlacePoint(db *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		currentUser, err := GetClaim(w, r)
		if err != nil {
			InternalServerError("Cannot fetch data about user: " + err.Error())
			return
		}

		var game Game
		db.Preload("Moves").First(&game, vars["id"])
		if game.PlayerOneID != uint(currentUser["id"].(float64)) && game.PlayerTwoID != uint(currentUser["id"].(float64)) {
			BadRequestError("It's not your game!").SendResponse(w)
			return
		}

		if game.IsFinished == true {
			BadRequestError("This game is finished").SendResponse(w)
			return
		}

		var lastMove Move
		if len(game.Moves) > 0 {
			lastMove = game.Moves[len(game.Moves)-1]
		}

		if lastMove.UserID == uint(currentUser["id"].(float64)) && lastMove.UserID != 0 {
			BadRequestError("It's not your turn!").SendResponse(w)
			return
		}

		var requestBody CreatePointRequest
		json.NewDecoder(r.Body).Decode(&requestBody)

		for i := range game.Moves {
			if game.Moves[i].PositionX == requestBody.X && game.Moves[i].PositionY == requestBody.Y {
				BadRequestError("This move exists!").SendResponse(w)
				return
			}
		}

		if requestBody.X < areaSizeMin || requestBody.X > areaSizeMax {
			BadRequestError(fmt.Sprintf("Invalid range for X: should be between %d and %d", areaSizeMin, areaSizeMax)).SendResponse(w)
			return
		}
		if requestBody.Y < areaSizeMin || requestBody.Y > areaSizeMax {
			BadRequestError(fmt.Sprintf("Invalid range for Y: should be between %d and %d", areaSizeMin, areaSizeMax)).SendResponse(w)
			return
		}

		move := Move{
			GameID:    game.ID,
			Game:      game,
			UserID:    uint(currentUser["id"].(float64)),
			PositionX: requestBody.X,
			PositionY: requestBody.Y,
		}
		db.Create(&move)
		UpdateGame(db, game.ID, uint(currentUser["id"].(float64)))

		json.NewEncoder(w).Encode(move)
	}
}
