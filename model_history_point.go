package main

import "time"

type HistoryPoint struct {
	ID        uint       `json:"id" gorm:"primary_key"`
	UserID    uint       `json:"-" gorm:"index"`
	User      User       `json:"user"`
	Before    uint       `json:"before"`
	After     uint       `json:"after"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" sql:"index"`
}

func (HistoryPoint) TableName() string {
	return "point_backlogs"
}
