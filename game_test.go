package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckCrossFromLeft(t *testing.T) {
	moves := []Move{
		Move{PositionX: 0, PositionY: 0, UserID: 3},
		Move{PositionX: 0, PositionY: 1, UserID: 4},
		Move{PositionX: 1, PositionY: 1, UserID: 3},
		Move{PositionX: 1, PositionY: 2, UserID: 4},
		Move{PositionX: 2, PositionY: 2, UserID: 3},
	}

	assert.True(t, checkCross(moves, 3))
	assert.False(t, checkCross(moves, 4))
}

func TestCheckCrossFromRight(t *testing.T) {
	moves := []Move{
		Move{PositionX: 2, PositionY: 0, UserID: 3},
		Move{PositionX: 0, PositionY: 0, UserID: 4},
		Move{PositionX: 1, PositionY: 1, UserID: 3},
		Move{PositionX: 0, PositionY: 1, UserID: 4},
		Move{PositionX: 0, PositionY: 2, UserID: 3},
	}

	assert.True(t, checkCross(moves, 3))
	assert.False(t, checkCross(moves, 4))
}

// Mispelled functionality, checkRowN checks column
func TestCheckColumn(t *testing.T) {
	moves := []Move{
		Move{PositionX: 0, PositionY: 0, UserID: 3},
		Move{PositionX: 1, PositionY: 0, UserID: 4},
		Move{PositionX: 0, PositionY: 1, UserID: 3},
		Move{PositionX: 1, PositionY: 1, UserID: 4},
		Move{PositionX: 0, PositionY: 2, UserID: 3},
	}

	assert.True(t, checkRowN(0, moves, 3))
	assert.False(t, checkRowN(0, moves, 4))
}

func TestCheckRow(t *testing.T) {
	moves := []Move{
		Move{PositionX: 0, PositionY: 0, UserID: 3},
		Move{PositionX: 0, PositionY: 1, UserID: 4},
		Move{PositionX: 1, PositionY: 0, UserID: 3},
		Move{PositionX: 1, PositionY: 1, UserID: 4},
		Move{PositionX: 2, PositionY: 0, UserID: 3},
	}

	assert.True(t, checkColumn(0, moves, 3))
	assert.False(t, checkColumn(0, moves, 4))
}

func TestFindWinner(t *testing.T) {
	playerOne := User{ID: 3}
	playeerTwo := User{ID: 4}

	moves := []Move{
		Move{PositionX: 0, PositionY: 0, UserID: 3},
		Move{PositionX: 0, PositionY: 1, UserID: 4},
		Move{PositionX: 1, PositionY: 0, UserID: 3},
		Move{PositionX: 1, PositionY: 1, UserID: 4},
		Move{PositionX: 2, PositionY: 0, UserID: 3},
	}

	game := Game{
		PlayerOne:   playerOne,
		PlayerOneID: playerOne.ID,
		PlayerTwo:   playeerTwo,
		PlayerTwoID: playeerTwo.ID,
		Moves:       moves,
	}

	winner, err := GetWinner(game, 3)

	assert.Nil(t, err)
	assert.Equal(t, playerOne, winner)
}
