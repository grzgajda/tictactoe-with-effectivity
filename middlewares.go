package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/context"
	padUtf8 "github.com/willf/pad/utf8"
)

const USER_CLAIMS = "auth_user"

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		if len(tokenString) == 0 {
			UnauthorizedError("Missing Authorization Header").SendResponse(w)
			return
		}
		tokenString = strings.Replace(tokenString, "Bearer ", "", 1)
		claims, err := VerifyJSONWebTokenLoggable(tokenString)
		if err != nil {
			UnauthorizedError("Error verifying JWT token: " + err.Error()).SendResponse(w)
			return
		}

		context.Set(r, USER_CLAIMS, claims)
		next.ServeHTTP(w, r)
	})
}

func JSONResponse(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")

		next.ServeHTTP(w, r)
	})
}

func CORSResponse(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		if r.Method == http.MethodOptions {
			w.WriteHeader(http.StatusOK)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		next.ServeHTTP(w, r)

		var timeDuration string
		timeDuration = fmt.Sprintf("[%.2f ms]", float64(time.Since(start).Nanoseconds()/1e4)/100.0)
		timeDuration = padUtf8.Right(timeDuration, 13, " ")

		log.Debugf(
			"%s %s %s (%s %s)",
			timeDuration,
			r.Method,
			r.RequestURI,
			"200",
			http.StatusText(http.StatusOK),
		)
	})
}
