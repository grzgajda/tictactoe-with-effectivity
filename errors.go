package main

import (
	"encoding/json"
	"net/http"
)

type ResponseError struct {
	StatusCode   int    `json:"-"`
	ErrorMessage string `json:"error"`
}

func (error ResponseError) SendResponse(w http.ResponseWriter) {
	w.WriteHeader(error.StatusCode)
	json.NewEncoder(w).Encode(error)
}

func BadRequestError(message string) ResponseError {
	return ResponseError{
		StatusCode:   http.StatusBadRequest,
		ErrorMessage: message,
	}
}

func InternalServerError(message string) ResponseError {
	return ResponseError{
		StatusCode:   http.StatusInternalServerError,
		ErrorMessage: message,
	}
}

func UnauthorizedError(message string) ResponseError {
	return ResponseError{
		StatusCode:   http.StatusUnauthorized,
		ErrorMessage: message,
	}
}

func ServiceUnavailableError(message string) ResponseError {
	return ResponseError{
		StatusCode:   http.StatusServiceUnavailable,
		ErrorMessage: message,
	}
}
