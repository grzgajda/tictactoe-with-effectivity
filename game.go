package main

import (
	"errors"

	"github.com/jinzhu/gorm"
)

const areaSizeMin = 0
const areaSizeMax = 2

func GetWinner(game Game, currentUser uint) (User, error) {
	for _, i := range []int{0, 1, 2} {
		if checkRowN(uint(i), game.Moves, game.PlayerOneID) == true {
			return game.PlayerOne, nil
		}
		if checkRowN(uint(i), game.Moves, game.PlayerTwoID) == true {
			return game.PlayerTwo, nil
		}

		if checkColumn(uint(i), game.Moves, game.PlayerOneID) == true {
			return game.PlayerOne, nil
		}
		if checkColumn(uint(i), game.Moves, game.PlayerTwoID) == true {
			return game.PlayerTwo, nil
		}
	}

	if checkCross(game.Moves, game.PlayerOneID) == true {
		return game.PlayerOne, nil
	}
	if checkCross(game.Moves, game.PlayerTwoID) == true {
		return game.PlayerTwo, nil
	}

	return User{}, errors.New("there is no winner yet")
}

func UpdateGame(db *gorm.DB, gameID uint, currentUser uint) {
	var game Game
	db.Preload("Moves").
		Preload("PlayerOne").
		Preload("PlayerTwo").
		First(&game, gameID)

	winner, err := GetWinner(game, currentUser)
	if err != nil {
		if len(game.Moves) == 9 {
			game.IsFinished = true
			db.Save(&game)
		}
		return
	}

	game.WinnerID = winner.ID
	game.Winner = winner
	game.IsFinished = true
	db.Save(&game)
}

func checkRowN(rowN uint, moves []Move, currentUserID uint) bool {
	var userMoves []Move

	for i := range moves {
		if moves[i].UserID == currentUserID {
			userMoves = append(userMoves, moves[i])
		}
	}

	var acceptedMoves []Move

	for j := range userMoves {
		if userMoves[j].PositionX == rowN {
			if userMoves[j].PositionY == 0 || userMoves[j].PositionY == 1 || userMoves[j].PositionY == 2 {
				acceptedMoves = append(acceptedMoves, userMoves[j])
			}
		}
	}

	return len(acceptedMoves) == 3
}

func checkColumn(columnN uint, moves []Move, currentUserID uint) bool {
	var userMoves []Move

	for i := range moves {
		if moves[i].UserID == currentUserID {
			userMoves = append(userMoves, moves[i])
		}
	}

	var acceptedMoves []Move
	for j := range userMoves {
		if userMoves[j].PositionY == columnN {
			if userMoves[j].PositionX == 0 || userMoves[j].PositionX == 1 || userMoves[j].PositionX == 2 {
				acceptedMoves = append(acceptedMoves, userMoves[j])
			}
		}
	}

	return len(acceptedMoves) == 3
}

func checkCross(moves []Move, currentUserID uint) bool {
	var userMoves []Move

	for i := range moves {
		if moves[i].UserID == currentUserID {
			userMoves = append(userMoves, moves[i])
		}
	}

	var acceptedMovesLeft []Move
	var acceptedMovesRight []Move

	for i := range userMoves {
		for _, x := range []int{0, 1, 2} {
			if userMoves[i].PositionX == uint(x) && userMoves[i].PositionY == uint(x) {
				acceptedMovesLeft = append(acceptedMovesLeft, userMoves[i])
			}
		}

		for x, y := range []int{2, 1, 0} {
			if userMoves[i].PositionY == uint(y) && userMoves[i].PositionX == uint(x) {
				acceptedMovesRight = append(acceptedMovesRight, userMoves[i])
			}
		}
	}

	return len(acceptedMovesLeft) == 3 || len(acceptedMovesRight) == 3
}
