package main

import (
	"database/sql/driver"
	"fmt"
	"reflect"
	"regexp"
	"time"
	"unicode"

	"github.com/gorilla/handlers"

	"github.com/jinzhu/gorm"
	padUtf8 "github.com/willf/pad/utf8"
)

var sqlRegexp = regexp.MustCompile(`(\$\d+)|\?`)

type SQLLoger struct {
	*gorm.Logger
}

type RecoveryHTTPLogger struct {
	handlers.RecoveryHandlerLogger
}

func (SQLLoger) Print(values ...interface{}) {
	if len(values) > 1 {
		milisecondTime := float64(values[2].(time.Duration).Nanoseconds()/1e4) / 100.0

		var timeDuration string
		timeDuration = fmt.Sprintf("[%.2f ms]", milisecondTime)
		timeDuration = padUtf8.Right(timeDuration, 13, " ")

		var sql string
		var formattedValues []string

		for _, value := range values[4].([]interface{}) {
			indirectValue := reflect.Indirect(reflect.ValueOf(value))
			if indirectValue.IsValid() {
				value = indirectValue.Interface()
				if t, ok := value.(time.Time); ok {
					formattedValues = append(formattedValues, fmt.Sprintf("\033[35m'%v'\033[0m", t.Format(time.RFC3339)))
				} else if b, ok := value.([]byte); ok {
					if str := string(b); isPrintable(str) {
						formattedValues = append(formattedValues, fmt.Sprintf("\033[35m'%v'\033[0m", str))
					} else {
						formattedValues = append(formattedValues, "\033[35m'<binary>'\033[0m")
					}
				} else if r, ok := value.(driver.Valuer); ok {
					if value, err := r.Value(); err == nil && value != nil {
						formattedValues = append(formattedValues, fmt.Sprintf("\033[35m'%v'\033[0m", value))
					} else {
						formattedValues = append(formattedValues, "NULL")
					}
				} else {
					formattedValues = append(formattedValues, fmt.Sprintf("\033[35m'%v'\033[0m", value))
				}
			} else {
				formattedValues = append(formattedValues, fmt.Sprintf("\033[35m'%v'\033[0m", value))
			}
		}

		var formattedValuesLength = len(formattedValues)
		for index, value := range sqlRegexp.Split(values[3].(string), -1) {
			sql += value
			if index < formattedValuesLength {
				sql += formattedValues[index]
			}
		}

		log.Debugf("%s %s", timeDuration, sql)
	}
}

func (RecoveryHTTPLogger) Println(v ...interface{}) {
	if len(v) > 0 {
		for i := range v {
			var errorMessage string
			errorMessage = fmt.Sprintf("Cannot execute HTTP request: %s", v[i])

			log.Errorf(errorMessage)
		}
	}
}

func isPrintable(s string) bool {
	for _, r := range s {
		if !unicode.IsPrint(r) {
			return false
		}
	}
	return true
}
