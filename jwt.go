package main

import (
	"fmt"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	padUtf8 "github.com/willf/pad/utf8"
)

var SigningKey = []byte(os.Getenv("APP_JWT_KEY"))

type JSONWebToken struct {
	Token string `json:"jwt_token"`
}

func GetJSONWebToken(id uint, username string, userslug string) (JSONWebToken, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":       id,
		"username": username,
		"userslug": userslug,
		"expire":   time.Now().Add(15 * time.Minute).Unix(),
	})
	tokenString, err := token.SignedString(SigningKey)

	return JSONWebToken{Token: tokenString}, err
}

func GetJSONWebTokenLoggable(id uint, username string, userslug string) (JSONWebToken, error) {
	start := time.Now()
	jwt, err := GetJSONWebToken(id, username, userslug)

	var timeDuration string
	timeDuration = fmt.Sprintf("[%.2f ms]", float64(time.Since(start).Nanoseconds()/1e4)/100.0)
	timeDuration = padUtf8.Right(timeDuration, 13, " ")

	if err == nil {
		log.Debugf("%s %s", timeDuration, fmt.Sprintf("JWT: Created JSON Web Token for User %s (ID: %d)", username, id))
	} else {
		log.Errorf("%s %s", timeDuration, fmt.Sprintf("JWT: Error during creating token for User %s (ID: %d): %s", username, id, err.Error()))
	}

	return jwt, err
}

func VerifyJSONWebToken(tokenString string) (jwt.Claims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return SigningKey, nil
	})
	if err != nil {
		return nil, err
	}

	return token.Claims, err
}

func VerifyJSONWebTokenLoggable(tokenString string) (jwt.Claims, error) {
	start := time.Now()
	claims, err := VerifyJSONWebToken(tokenString)

	var timeDuration string
	timeDuration = fmt.Sprintf("[%.2f ms]", float64(time.Since(start).Nanoseconds()/1e4)/100.0)
	timeDuration = padUtf8.Right(timeDuration, 13, " ")

	if err == nil {
		log.Debugf("%s %s", timeDuration, fmt.Sprintf("JWT: Verified JSON Web Token %s...", tokenString[0:20]))
	} else {
		log.Errorf("%s %s", timeDuration, fmt.Sprintf("JWT: Cannot verify JSON Web Token %s...: %s", tokenString[0:20], err.Error()))
	}

	return claims, err
}
