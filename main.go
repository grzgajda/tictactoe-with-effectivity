package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/joho/godotenv/autoload"
	"github.com/op/go-logging"
)

var log = logging.MustGetLogger("example")
var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05} ▶ %{level:.6s}%{color:reset} %{message}`,
)

func main() {
	backend := logging.NewLogBackend(os.Stderr, "", 0)
	backendFormatter := logging.NewBackendFormatter(backend, format)
	logging.SetBackend(backendFormatter)

	var shouldDebug bool
	if os.Getenv("APP_ENV") == "dev" {
		shouldDebug = true
	}

	dbConn := fmt.Sprintf(
		"%s:%s@/%s?charset=utf8&parseTime=True&loc=Local",
		os.Getenv("API_MYSQL_USER"),
		os.Getenv("API_MYSQL_PASS"),
		os.Getenv("API_MYSQL_NAME"),
	)
	db, err := gorm.Open("mysql", dbConn)
	if err != nil {
		log.Fatalf("cannot open mysql connection: %s", err)
	}
	db.SetLogger(SQLLoger{})
	db.LogMode(shouldDebug)
	db.AutoMigrate(
		&User{},
		&HistoryPoint{},
		&Game{},
		&Move{},
	)

	defer db.Close()

	http.ListenAndServe(
		fmt.Sprintf(":%s", os.Getenv("API_HTTP_PORT")),
		CreateCORSHandler(NewRouter(db, shouldDebug), shouldDebug),
	)
}
