package main

import "time"

type Move struct {
	ID        uint       `json:"id" gorm:"primary_key"`
	GameID    uint       `json:"-"`
	Game      Game       `json:"game"`
	UserID    uint       `json:"-"`
	User      User       `json:"user"`
	PositionX uint       `json:"x"`
	PositionY uint       `json:"y"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" sql:"index"`
}
