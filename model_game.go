package main

import "time"

type Game struct {
	ID          uint       `json:"id" gorm:"primary_key"`
	IsFinished  bool       `json:"is_finished"`
	WinnerID    uint       `json:"-"`
	PlayerOneID uint       `json:"-"`
	PlayerTwoID uint       `json:"-"`
	Winner      User       `json:"winner,omitempty"`
	PlayerOne   User       `json:"player_one"`
	PlayerTwo   User       `json:"player_two"`
	Moves       []Move     `json:"moves"`
	CreatedAt   time.Time  `json:"-"`
	UpdatedAt   time.Time  `json:"-"`
	DeletedAt   *time.Time `json:"-" sql:"index"`
}
