package main

import (
	"net/http"

	"github.com/jinzhu/gorm"
)

type RouteHandler func(*gorm.DB) http.HandlerFunc

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc RouteHandler
	IsPrivate   bool
}

var routes = []Route{
	Route{
		Name:        "AUTH",
		Method:      http.MethodPost,
		Pattern:     "/auth",
		HandlerFunc: Authenticate,
	},
	Route{
		Name:        "GET_USERS",
		Method:      http.MethodGet,
		Pattern:     "/users",
		HandlerFunc: IndexUsers,
	},
	Route{
		Name:        "GET_AUTH_USER",
		Method:      http.MethodGet,
		Pattern:     "/me",
		HandlerFunc: IndexCurrentUser,
		IsPrivate:   true,
	},
	Route{
		Name:        "GET_GAMES",
		Method:      http.MethodGet,
		Pattern:     "/games",
		HandlerFunc: IndexGames,
		IsPrivate:   true,
	},
	Route{
		Name:        "CREATE_GAME",
		Method:      http.MethodPost,
		Pattern:     "/games",
		HandlerFunc: CreateGame,
		IsPrivate:   true,
	},
	Route{
		Name:        "PLACE_POINT",
		Method:      http.MethodPost,
		Pattern:     "/games/{id}",
		HandlerFunc: PlacePoint,
		IsPrivate:   true,
	},
}
