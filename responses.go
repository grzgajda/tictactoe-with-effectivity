package main

type AuthenticationRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type IndexUsersResponse struct {
	ID       uint   `json:"id"`
	Username string `json:"username"`
	Slug     string `json:"slug"`
	Points   int64  `json:"points"`
}

type CreatePointRequest struct {
	X uint `json:"x"`
	Y uint `json:"y"`
}

type CreateGameRequest struct {
	PlayWithID uint `json:"play_with"`
}
