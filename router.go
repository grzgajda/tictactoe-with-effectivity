package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/handlers"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func NewRouter(db *gorm.DB, shouldDebug bool) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc(db)
		handler = JSONResponse(handler)
		handler = CORSResponse(handler)

		if shouldDebug == true {
			handler = LoggingMiddleware(handler)

			recoveryLogger := handlers.RecoveryLogger(RecoveryHTTPLogger{})
			recoveryHandler := handlers.RecoveryHandler(recoveryLogger)
			handler = recoveryHandler(handler)
		}
		if route.IsPrivate == true {
			handler = AuthMiddleware(handler)
		}

		router.
			Methods(http.MethodOptions).
			Path(route.Pattern).
			Name(fmt.Sprintf("cors_%s", route.Name)).
			Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Header().Add("Access-Control-Allow-Origin", "*")
				w.Header().Set("Access-Control-Allow-Origin", "*")
			}))

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}
