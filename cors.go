package main

import (
	"net/http"

	"github.com/rs/cors"
)

func CreateCORSHandler(handler http.Handler, shouldDebug bool) http.Handler {
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodOptions,
		},
		AllowedHeaders: []string{"Authorization"},
		Debug:          false,
	})

	return c.Handler(handler)
}
